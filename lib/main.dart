import 'package:flutter/material.dart';
import 'package:industrie_calculator_app/model/calculator_model.dart';
import 'package:industrie_calculator_app/provider/calculation_provider.dart';
import 'package:industrie_calculator_app/screens/calculator_screen.dart';
import 'package:industrie_calculator_app/screens/calculator_screen2.dart';
import 'package:provider/provider.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CalculatorModel>(
        create: (_) => CalculatorModel(),
        child: MaterialApp(
          onGenerateTitle: (context) {
            return AppLocalizations.of(context)!.appTitle;
          },
          localizationsDelegates: const [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate
          ],
          supportedLocales: const [
            Locale('en'),
            Locale('de')
          ],
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
              primarySwatch: Colors.blue,
              fontFamily: 'Roboto',
              textTheme: TextTheme(
                bodyMedium: TextStyle(fontSize: 18),
              )),
          home: CalculatorScreen2(),
        ));
  }
}
