class CalculatorButton {
  final String label;
  final bool isOperator, isEqualSign;
  double? width, height;

  CalculatorButton(
    this.label, {
    this.isOperator = false,
    this.isEqualSign = false,
    this.width,
    this.height,
  });
}

Map<String, CalculatorButton> buttonMap = {
  '7': CalculatorButton('7'),
  '8': CalculatorButton('8'),
  '9': CalculatorButton('9'),
  '⌫': CalculatorButton('⌫', width: 20, height: 80),
  '4': CalculatorButton('4'),
  '5': CalculatorButton('5'),
  '6': CalculatorButton('6'),
  '0': CalculatorButton('0'),
  '1': CalculatorButton('1'),
  '2': CalculatorButton('2'),
  ' + ': CalculatorButton(' + ', isOperator: true),
  '3':CalculatorButton('3'),
  '.':CalculatorButton('.', isOperator: true),
  'AC':CalculatorButton('AC'),
  ' - ':CalculatorButton(' - ', isOperator: true),
  '=':CalculatorButton('=', isEqualSign: true, width: 20, height: 100)
};
