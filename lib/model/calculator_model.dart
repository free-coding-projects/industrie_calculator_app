import 'package:flutter/widgets.dart';
import 'package:industrie_calculator_app/model/command/comma_command.dart';
import 'package:industrie_calculator_app/model/command/command_interface.dart';
import 'package:industrie_calculator_app/model/command/digits/digit_1.dart';
import 'package:industrie_calculator_app/model/command/digits/digit_2.dart';
import 'package:industrie_calculator_app/model/command/digits/digit_3.dart';
import 'package:industrie_calculator_app/model/command/digits/digit_4.dart';
import 'package:industrie_calculator_app/model/command/digits/digit_5.dart';
import 'package:industrie_calculator_app/model/command/digits/digit_6.dart';
import 'package:industrie_calculator_app/model/command/digits/digit_7.dart';
import 'package:industrie_calculator_app/model/command/digits/digit_8.dart';
import 'package:industrie_calculator_app/model/command/digits/digit_9.dart';
import 'package:industrie_calculator_app/model/command/operator/operator_equals.dart';
import 'package:industrie_calculator_app/model/command/operator/operator_minus.dart';
import 'package:industrie_calculator_app/model/command/operator/operator_plus.dart';
import 'package:industrie_calculator_app/provider/calculation_provider.dart';
import 'package:intl/intl.dart';
import 'package:math_expressions/math_expressions.dart';

class CalculatorModel extends ChangeNotifier {
  String input = "";
  String result = '';
  String resultInHours = '';
  List<Command> commands = [];
  Map<String, Command> commandMapping = {
    '1': OneCommand(),
    '2': TwoCommand(),
    '3': ThreeCommand(),
    '4': FourCommand(),
    '5': FiveCommand(),
    '6': SixCommand(),
    '7': SevenCommand(),
    '8': EightCommand(),
    '9': NineCommand(),
    '+': PlusCommand(),
    '-': MinusCommand(),
    ',': CommaCommand(),
    '=': EqualsCommand(),
  };

  void addCommand(Command command) {
    commands.add(command);
  }

  void removeLastCommand(){
    commands.removeLast();
  }

  void calculateExpression(){
    if (input != "") {
      Parser p = Parser();
      Expression exp = p.parse(input);
      ContextModel cm = ContextModel();
      result = '${exp.evaluate(EvaluationType.REAL, cm)}';
      resultInHours = calculateInNormalHoursAndMinutes(result);
    }
  }

  String calculateInNormalHoursAndMinutes(String result){
    NumberFormat formatter = NumberFormat("00");
    double timestamp = double.parse(result);

    int inHours = timestamp.toInt();
    double inMinutes = (timestamp - inHours).toPrecision(2) * 60;
    return "${formatter.format(inHours)}:${formatter.format(inMinutes)}";
  }

  bool isLastCharOperator(String input){
    return input.endsWith(" + ") || input.endsWith(" - ") || input.endsWith(" = ");
  }

  bool containsCurrentNumberComma(String input){
    List<String> parts = input.split(RegExp(r'[\s+-]'));
    String currentNumber = parts.isNotEmpty ? parts.last : '';

    return currentNumber.contains(",");
  }

  void buttonPressed(String button) {

  }
}
