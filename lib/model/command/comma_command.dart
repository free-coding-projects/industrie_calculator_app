import 'package:industrie_calculator_app/model/calculator_model.dart';

import 'command_interface.dart';

class CommaCommand extends Command {
  @override
  String getCommandCmdText() {
    return ',';
  }

  @override
  void undoCommand(CalculatorModel model) {
    model.input = model.input.substring(0, model.input.length - 1);
  }

  @override
  void redoCommand(CalculatorModel model){
    model.input += getCommandCmdText();
  }
}
