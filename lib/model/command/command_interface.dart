import 'package:industrie_calculator_app/model/calculator_model.dart';

abstract class Command {
  String getCommandCmdText();
  void undoCommand(CalculatorModel model);
  void redoCommand(CalculatorModel model);
}
