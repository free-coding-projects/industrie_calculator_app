import 'package:industrie_calculator_app/model/command/digits/digit_command.dart';

class ThreeCommand extends DigitCommand {
  @override
  String getCommandCmdText() {
    return '3';
  }
}
