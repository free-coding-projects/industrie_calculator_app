import 'package:industrie_calculator_app/model/command/digits/digit_command.dart';

class FourCommand extends DigitCommand {
  @override
  String getCommandCmdText() {
    return '4';
  }
}
