import '../../calculator_model.dart';
import '../command_interface.dart';

abstract class DigitCommand extends Command {
  @override
  void undoCommand(CalculatorModel model) {
    model.input = model.input.substring(0, model.input.length - 1);
  }

  @override
  void redoCommand(CalculatorModel model){
    model.input += getCommandCmdText();
  }
}
