import '../../calculator_model.dart';
import '../command_interface.dart';

abstract class OperatorCommand extends Command {
  @override
  void undoCommand(CalculatorModel model) {
    model.input = model.input.substring(0, model.input.length - 3);
  }

  @override
  void redoCommand(CalculatorModel model){
    model.input += getCommandCmdText();
  }
}
