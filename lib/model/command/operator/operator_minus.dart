import 'package:industrie_calculator_app/model/command/operator/operator_command.dart';

class MinusCommand extends OperatorCommand {
  @override
  String getCommandCmdText() {
    return ' - ';
  }
}
