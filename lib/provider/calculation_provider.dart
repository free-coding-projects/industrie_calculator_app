import 'dart:developer' as dev;
import 'package:intl/intl.dart';
import 'package:math_expressions/math_expressions.dart';
import 'package:flutter/widgets.dart';
import 'package:industrie_calculator_app/model/calculator_button.dart';
import 'dart:math';

extension Precision on double {
  double toPrecision(int fractionDigits) {
    var mod = pow(10, fractionDigits.toDouble()).toDouble();
    return ((this * mod).round().toDouble() / mod);
  }
}

class CalculationProvider with ChangeNotifier {
  String equation = '';
  String result = '';
  String resultInHours = '';

  bool operatorOpened = false;
  bool commaOpened = false;

  void addToEquation(String sign) {
    switch (sign) {
      case 'AC':
        equation = '';
        result = '';
        resultInHours = '';
        commaOpened = false;
        break;
      case "⌫":
        if (equation != '') {
          if (equation.endsWith(' ')) {
            equation = equation.substring(0, equation.length - 3);
          } else {
            equation = equation.substring(0, equation.length - 1);
          }
        }
        break;
      case '.':
        var button = getButton(equation);
        if (!commaOpened && button != null) {
          equation += sign;
          commaOpened = true;
        }
        break;
      case ' + ':
      case ' - ':
        var button = getButton(equation);
        if (operatorOpened && button != null && button.isOperator) {
          equation = equation.substring(0, equation.length - 1);
        } else if (!operatorOpened) {
          equation += sign;
          operatorOpened = true;
          commaOpened = false;
        }
        break;
      case "=":
        if (operatorOpened) {
          equation = equation.substring(0, equation.length - 1);
          operatorOpened = false;
        }

        if (equation != null) {
          Parser p = Parser();
          Expression exp = p.parse(equation);
          ContextModel cm = ContextModel();
          result = '${exp.evaluate(EvaluationType.REAL, cm)}';
          resultInHours = calculateInNormalHoursAndMinutes(result);
        }
        break;
      case "0":
      case "1":
      case "2":
      case "3":
      case "4":
      case "5":
      case "6":
      case "7":
      case "8":
      case "9":
        operatorOpened = false;
        equation += sign;
    }
    notifyListeners();
  }

  CalculatorButton? getButton(String equation) {
    if (equation.isNotEmpty) {
      var lastSign = equation[equation.length - 1];
      return buttonMap[lastSign];
    }
  }

  String calculateInNormalHoursAndMinutes(String result){
    NumberFormat formatter = NumberFormat("00");
    double timestamp = double.parse(result);

    int inHours = timestamp.toInt();
    double inMinutes = (timestamp - inHours).toPrecision(2) * 60;
    return "${formatter.format(inHours)}:${formatter.format(inMinutes)}";
  }
}
