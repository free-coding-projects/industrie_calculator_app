import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:industrie_calculator_app/provider/calculation_provider.dart';
import 'package:provider/provider.dart';
import '../model/calculator_button.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CalculatorScreen extends StatefulWidget {
  const CalculatorScreen({super.key});

  @override
  State<CalculatorScreen> createState() => _CalculatorScreenState();
}

class _CalculatorScreenState extends State<CalculatorScreen> {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    final calculatorProvider =
        Provider.of<CalculationProvider>(context, listen: false);

    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.appTitle),
      ),
      body: Column(
        children: [
          Container(
            width: mediaQuery.width,
            height: mediaQuery.height * .6,
            padding: EdgeInsets.symmetric(
              vertical: mediaQuery.width * 0.08,
              horizontal: mediaQuery.width * 0.06,
            ),
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 3),
                  borderRadius: BorderRadius.circular(10.0),
                  gradient: const LinearGradient(
                      colors: [Colors.black38, Colors.black]),
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black45,
                        blurRadius: 2.0,
                        offset: Offset(2.0, 2.0))
                  ]),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    height: mediaQuery.height * 0.1,
                    child: ListView(
                      reverse: true,
                      scrollDirection: Axis.vertical,
                      children: [
                        Container(
                          child: Consumer<CalculationProvider>(
                            builder: (context, equation, child) => Text(
                              equation.equation,
                              style: const TextStyle(color: Colors.white),
                              softWrap: true,
                              maxLines: 4,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  const SizedBox(height: 32.0),
                  Container(
                    width: mediaQuery.width,
                    margin: const EdgeInsets.all(2.0),
                    padding: const EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                        border: Border.all(), color: Colors.blueAccent),
                    child: Row(
                      children: [
                        Text(
                          '${AppLocalizations.of(context)!.labelIndustrialtime}: ',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                        Consumer<CalculationProvider>(
                          builder: (context, equation, child) => Text(
                            equation.result,
                            style: const TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 12.0),
                  Container(
                    width: mediaQuery.width,
                    margin: const EdgeInsets.all(2.0),
                    padding: const EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                        border: Border.all(), color: Colors.greenAccent),
                    child: Row(
                      children: [
                        Text('${AppLocalizations.of(context)!.labelNormalTime}: ',
                            style: Theme.of(context).textTheme.bodyMedium),
                        Consumer<CalculationProvider>(
                          builder: (context, equation, child) => Text(
                            equation.resultInHours,
                            style: const TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: StaggeredGrid.count(
              axisDirection: AxisDirection.down,
              crossAxisCount: 4,
              mainAxisSpacing: 4,
              crossAxisSpacing: 4,
              children: [...createCalculationButtons(calculatorProvider)],
            ),
          )
        ],
      ),
    );
  }
}

ElevatedButton createButton(
    {required CalculatorButton button,
    required CalculationProvider calculationProvider}) {
  return ElevatedButton(
    style: ElevatedButton.styleFrom(
        minimumSize: button.width != null && button.height != null
            ? Size(button.width!, button.height!)
            : null),
    onPressed: calculationProvider.commaOpened && button.label == '.'
        ? null
        : () {
            calculationProvider.addToEquation(button.label);
          },
    child: Text(
      button.label,
      style: const TextStyle(
        color: Colors.white,
        fontSize: 28,
        fontWeight: FontWeight.w200,
      ),
    ),
  );
}

List<StaggeredGridTile> createCalculationButtons(
    CalculationProvider calculationProvider) {
  List<StaggeredGridTile> createdButtons = [];
  for (var button in buttonMap.values) {
    createdButtons.add(StaggeredGridTile.fit(
      crossAxisCellCount: 1,
      child: createButton(
          button: button, calculationProvider: calculationProvider),
    ));
  }
  return createdButtons;
}
