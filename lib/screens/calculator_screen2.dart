import 'package:flutter/material.dart';
import 'package:industrie_calculator_app/model/calculator_model.dart';
import 'package:provider/provider.dart';

class CalculatorScreen2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Calculator New"),
      ),
      body: Column(
        children: [
          Consumer<CalculatorModel>(
            builder: (context, model, _) => Expanded(
              child: Container(
                alignment: Alignment.bottomRight,
                padding: const EdgeInsets.all(20.0),
                child:
                    Text(model.input, style: const TextStyle(fontSize: 24.0)),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _buildButton(context, '7',
                  () => context.read<CalculatorModel>().buttonPressed('7')),
              _buildButton(context, '8',
                      () => context.read<CalculatorModel>().buttonPressed('8')),
              _buildButton(context, '9',
                      () => context.read<CalculatorModel>().buttonPressed('9')),
              _buildButton(context, ',',
                      () => context.read<CalculatorModel>().buttonPressed(',')),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _buildButton(context, '4',
                      () => context.read<CalculatorModel>().buttonPressed('4')),
              _buildButton(context, '5',
                      () => context.read<CalculatorModel>().buttonPressed('5')),
              _buildButton(context, '6',
                      () => context.read<CalculatorModel>().buttonPressed('6')),
              _buildButton(context, '+',
                      () => context.read<CalculatorModel>().buttonPressed('+')),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _buildButton(context, '1',
                      () => context.read<CalculatorModel>().buttonPressed('1')),
              _buildButton(context, '2',
                      () => context.read<CalculatorModel>().buttonPressed('2')),
              _buildButton(context, '3',
                      () => context.read<CalculatorModel>().buttonPressed('3')),
              _buildButton(context, '-',
                      () => context.read<CalculatorModel>().buttonPressed('-')),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _buildButton(context, '0',
                      () => context.read<CalculatorModel>().buttonPressed('0')),
              _buildButton(context, 'C',
                      () => context.read<CalculatorModel>().buttonPressed('C')),
              _buildButton(context, '<-',
                      () => context.read<CalculatorModel>().buttonPressed('')),
              _buildButton(context, '=',
                      () => context.read<CalculatorModel>().buttonPressed('=')),
            ],
          ),
        ],
      ),
    );
  }
}

Widget _buildButton(BuildContext context, String text, VoidCallback onPressed,
    [bool isEnabled = true]) {
  return Expanded(
    child: TextButton(
        onPressed: isEnabled ? onPressed : null,
        child: Text(
          text,
          style: const TextStyle(fontSize: 20.0),
        )),
  );
}
